CREATE OR REPLACE DATABASE mafia_db;

CREATE OR REPLACE TABLE mafia_db.members (	
	dni VARCHAR (15) NOT NULL,
    name VARCHAR (128) NOT NULL,
    has_boss VARCHAR (5),
    is_boss VARCHAR (5),
    active VARCHAR (5),
    date_start DATE,
    level_id INT,
    group_id INT,
    prev_boss VARCHAR(15),
    PRIMARY KEY (dni)
);