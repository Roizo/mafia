#!/usr/bin/env bash

HOST=127.0.0.1
PORT=3306
DATABASE=PRUEBA_DB
DBUSER=root
DBPASSWORD=mypass
# Create new database schema
echo ".....Creating database schema mafia_db"
mysql -u$DBUSER -h $HOST -P $PORT -p$DBPASSWORD -e"source sql/create_database.sql;" 