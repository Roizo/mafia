package utils

import java.io.FileInputStream
import java.sql.{Connection, Statement}
import java.util.Properties

import org.slf4j.{Logger, LoggerFactory}

object UtilsDB {
  private val logger: Logger = LoggerFactory.getLogger(UtilsDB.getClass)

  //Load properties
  val prop = new Properties()
  prop.load(new FileInputStream(getClass.getClassLoader.getResource("config/local.properties").getPath))

  //Constants
  val DB_URL: String = prop.getProperty("url")
  val USER: String = prop.getProperty("user")
  val PASS: String = prop.getProperty("password")


  def connectDB(): Connection = {
    import java.sql._

    var conn: Connection = null

    try {
      Class.forName("org.mariadb.jdbc.Driver")
      logger.info("Connecting to a selected database...")
      conn = DriverManager.getConnection(DB_URL, USER, PASS)
      logger.info("Connected database successfully...")
      logger.info("Deleting table in given database...")
      conn
    }

  }
  def dropTable(tableName: String) = {
    var stmt: Statement = null;

    try {
      val conn = connectDB()
      stmt = conn.createStatement();
      val sql: String = s"DROP TABLE ${tableName} ";
      stmt.executeUpdate(sql);
      logger.info(s"Table ${tableName} deleted in given database...");
    } catch {
      case e: Exception => logger.info("exception caught: " + e);
    }
  }

  def renameTable(tableName: String, newTableName: String) = {
    var stmt: Statement = null;

    try {
      val conn = connectDB()
      stmt = conn.createStatement();
      val sql: String = s"RENAME TABLE `${tableName}` TO `${newTableName}`"
      stmt.executeUpdate(sql);
      logger.info(s"Table ${tableName} rename to ${newTableName} in given database...");
    } catch {
      case e: Exception => logger.info("exception caught: " + e);
    }
  }
}
