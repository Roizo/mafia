package mafia.models

case class MafiaMember (
    dni: String,
    name: String,
    has_boss: String,
    is_boss: String,
    active: String,
    date_start: String,
    level_id: String,
    group_id: String,
    prev_boss: String
                       )