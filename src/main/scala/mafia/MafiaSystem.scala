package mafia

import java.io.FileInputStream
import java.util.Properties

import utils.UtilsDB.{dropTable,renameTable}
import mafia.models.MafiaMember
import mafia.OpMafia.{insertData,deactivateMember,activateMember, topBoss}
import org.apache.spark.sql.{DataFrame, Dataset, SaveMode, SparkSession}
import org.rogach.scallop.{ScallopConf, ScallopOption}
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try

object MafiaSystem {
  private val logger: Logger = LoggerFactory.getLogger(MafiaSystem.getClass)

  class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
    val code_operation: ScallopOption[Int] = opt[Int](required = true)
    val data_path: ScallopOption[String] = opt[String]()
    val dni_member: ScallopOption[String] = opt[String]()
    verify()
  }

  def main(args: Array[String]): Unit = {
    val conf = new Conf(args) // Note: This line also works for "object Main extends App"
    //Get arguments
    val dataPath= Try(conf.data_path())
    val codeOperation = conf.code_operation()
    val dniMember = Try(conf.dni_member())
    //Load properties
    val prop = new Properties()
    prop.load(new FileInputStream(getClass.getClassLoader.getResource("config/local.properties").getPath))

    //Init SparkSession
    val sparkSession = SparkSession.builder.
      master("local[*]").
      appName("MafiaSystem").getOrCreate()


     codeOperation match {
      case 1 => //Insert data
        if(dataPath.isSuccess) insertData(sparkSession,dataPath.get, prop)
        else logger.error("Error: Required option 'data_path' not found")
      case 2 =>
        if(dniMember.isSuccess) deactivateMember(sparkSession,prop,dniMember.get)
        else logger.error("Error: Required option 'dni_member' not found")
      case 3 =>
        if(dniMember.isSuccess) activateMember(sparkSession,prop,dniMember.get)
        else logger.error("Error: Required option 'dni_member' not found")
      case 4 =>
        if(dniMember.isSuccess) topBoss(sparkSession,prop)
        else logger.error("Error: Required option 'dni_member' not found")
    }

  }







}
