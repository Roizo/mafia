package mafia

import java.util.Properties

import mafia.models.{LevelGroup, MafiaMember}
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}
import org.slf4j.{Logger, LoggerFactory}
import utils.UtilsDB.{dropTable, renameTable}

import scala.util.Try

object OpMafia {
  private val logger: Logger = LoggerFactory.getLogger(MafiaSystem.getClass)

  def insertData(sparkSession: SparkSession,dataPath: String, prop: Properties) = {
    import sparkSession.implicits._

    val df = sparkSession
      .read
      .option("header", "true")
      .option("delimiter", ";")
      .csv(dataPath)
      .toDF()
      .as[MafiaMember]

    df.write.mode(SaveMode.Overwrite).jdbc(prop.getProperty("url"), prop.getProperty("table_name"), prop)
  }

  def deactivateMember(sparkSession: SparkSession, prop: Properties,dni: String) : Boolean = {
    import sparkSession.implicits._

    //Read table mysql
    val all_mebmers_sql = sparkSession.read.jdbc(prop.getProperty("url"), prop.getProperty("table_name"), prop).toDF().as[MafiaMember]

    //Create spark table members mafia
    all_mebmers_sql.createOrReplaceTempView("members")

    //Get member to update
    val member_update = Try(sparkSession.sql("SELECT * FROM members WHERE dni = " + dni).as[MafiaMember].first())

    //Check member its found
    if (!member_update.isSuccess) {
      logger.error("'dni_member' not found")
      return false
    }

    //Update member, only the member
    val df_updated = all_mebmers_sql.map(x=>
    if(x.dni==member_update.get.dni)
    {MafiaMember(x.dni, x.name, x.has_boss, x.is_boss, "false", x.date_start, x.level_id,x.group_id, x.prev_boss)}
    else x)

    //If member is boss, find new boss
    if(member_update.get.is_boss == "true") {

      //Find another boss same level
      val new_boss = Try(sparkSession.sql("SELECT * FROM members " +
        "WHERE level_id = '" + member_update.get.level_id +
        "' AND group_id != '" + member_update.get.group_id +
        "' AND is_boss = 'true'" +
        " AND active = 'true'" +
        " ORDER BY date_start ASC")
        .as[MafiaMember].first())

      if(new_boss.isSuccess) {
        val result = df_updated.map(x=>
          //Change the group_id to the group_id of new boss and save old_boss
          if(x.level_id==member_update.get.level_id && x.group_id==member_update.get.group_id && x.dni != member_update.get.dni)
          {MafiaMember(x.dni, x.name, x.has_boss, x.is_boss, x.active, x.date_start, x.level_id,new_boss.get.group_id, member_update.get.dni)}
          //Not modify the other members
          else x)

        //Persist new df to mysql
        result.write.mode(SaveMode.Overwrite).jdbc(prop.getProperty("url"), "members_aux", prop)
        dropTable("members")
        renameTable("members_aux", "members")
      }
      else {
        //If not found another boss same level, the next boss will be the oldest direct subordinate of the previous boss
        val new_boss_next_option = Try(sparkSession.sql("SELECT * FROM members " +
          "WHERE level_id = '" + member_update.get.level_id +
          "' AND group_id = '" + member_update.get.group_id +
          "' AND active = 'true'" +
          " ORDER BY date_start ASC")
          .as[MafiaMember].first())

        if(new_boss_next_option.isSuccess) {
          val result:Dataset[MafiaMember] = df_updated.map(x=>
          //Activate new boss and save old boss
          if(x.dni==new_boss_next_option.get.dni)
          {MafiaMember(x.dni, x.name, x.has_boss, "true", x.active, x.date_start, x.level_id,x.group_id, member_update.get.dni)}
          //Save old boss to the other members
          else if(x.level_id==member_update.get.level_id && x.group_id==member_update.get.group_id)
          {MafiaMember(x.dni, x.name, x.has_boss, x.is_boss, x.active, x.date_start, x.level_id,x.group_id, member_update.get.dni)}
          //Not modify the other members
            else x)

          //Persist new df to mysql
          result.write.mode(SaveMode.Overwrite).jdbc(prop.getProperty("url"), "members_aux", prop)
          dropTable("members")
          renameTable("members_aux", "members")
        }
        else {
          logger.error("Error: New Boss not found")
          return false
        }
      }
    }
    else{
      //If not boss write only the changes of member
      df_updated.write.mode(SaveMode.Overwrite).jdbc(prop.getProperty("url"), "members_aux", prop)
      dropTable("members")
      renameTable("members_aux", "members")
    }
    true
  }

  def activateMember(sparkSession: SparkSession, prop: Properties,dni: String) : Boolean = {
    import sparkSession.implicits._

    //Read table mysql
    val all_mebmers_sql = sparkSession.read.jdbc(prop.getProperty("url"), prop.getProperty("table_name"), prop).toDF().as[MafiaMember]

    //Create spark table members mafia
    all_mebmers_sql.createOrReplaceTempView("members")

    //Get member to update
    val member_update = Try(sparkSession.sql("SELECT * FROM members WHERE dni = " + dni).as[MafiaMember].first())

    //Check member its found
    if (!member_update.isSuccess) {
      logger.error("'dni_member' not found")
      return false
    }

    //Update member, only the member
    val df_updated = all_mebmers_sql.map(x=>
      if(x.dni==member_update.get.dni)
      {MafiaMember(x.dni, x.name, x.has_boss, x.is_boss, "true", x.date_start, x.level_id,x.group_id, x.prev_boss)}
      else x)

    //If member is boss, deactivate old boss and return the subordinate to the boss updated
    if(member_update.get.is_boss == "true") {
        val result = df_updated.map(x=>
          //Change the group_id to the group_id of new boss and save old_boss
          if(x.prev_boss==member_update.get.dni && x.is_boss == "false")
          {MafiaMember(x.dni, x.name, x.has_boss, x.is_boss, x.active, x.date_start, member_update.get.level_id,member_update.get.group_id, "false")}
            //Deactivate old_boss
          else if(x.level_id == member_update.get.level_id && x.group_id == member_update.get.group_id
            && x.is_boss == "true" && x.dni != member_update.get.dni){
            MafiaMember(x.dni, x.name, x.has_boss, "false", x.active, x.date_start, x.level_id,x.group_id, "false")}
          //Not modify the other members
          else x)

        //Persist new df to mysql
        result.write.mode(SaveMode.Overwrite).jdbc(prop.getProperty("url"), "members_aux", prop)
        dropTable("members")
        renameTable("members_aux", "members")
      }
    else{
      //If not boss write only the changes of member
      df_updated.write.mode(SaveMode.Overwrite).jdbc(prop.getProperty("url"), "members_aux", prop)
      dropTable("members")
      renameTable("members_aux", "members")
    }
    true
  }

  def topBoss (sparkSession: SparkSession, prop: Properties) : Boolean = {
    import sparkSession.implicits._

    //Read table mysql
    val all_mebmers_sql = sparkSession.read.jdbc(prop.getProperty("url"), prop.getProperty("table_name"), prop).toDF().as[MafiaMember]

    //Create spark table members mafia
    all_mebmers_sql.createOrReplaceTempView("members")

    val query1 = sparkSession.sql("SELECT level_id, group_id,count(1) AS count_members from members GROUP BY group_id, level_id")

    query1.createOrReplaceTempView("subquery")
    //Get member to update
    val levels_groups_top = Try(sparkSession.sql
    ("SELECT level_id, group_id\nFROM \n" +
      "subquery WHERE count_members >50"))

    //Check member its found
    if (!levels_groups_top.isSuccess) {
      logger.error("'top boss' not found")
      return false
    }

    val level_group_array = levels_groups_top.get.toDF("level_id", "group_id").as[LevelGroup].collect()

    var result: Dataset[MafiaMember] = null
    for(level_group <- level_group_array){
      val df_updated = all_mebmers_sql.filter(x=> x.level_id==level_group.level_id && x.group_id==level_group.group_id && x.is_boss == "true")
      Try(result = result.union(df_updated)) getOrElse(result = df_updated)
    }
    print("Show boss +50 subordinates")
    result.show(result.count().toInt)
    true
  }

}
