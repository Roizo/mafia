# Proyecto Mafia

### Stack tecnológico utilizado

- Docker
- Scala
- Spark
- Maven
- MariaDB

### Desarrollo

Para el desarrollo de este proyecto he utilizado el IDE IntelliJ para el código en Scala y he utilizado un Docker de MariaDB para instalar una BBDD local.

### Iniciar entorno

-Crear docker con MariaDB
    
    docker run --name mariadbtest -p 3306:3306 -e MYSQL_ROOT_PASSWORD=mypass -d mariadb

-Crear BBDD

Es un script sh con las instrucciones simples SQL para iniciar la BBDD y crear la única tabla necesario para usar el programa. Tiene todas las rutas hardcodeadas a localhost y los nombres de BBDD y tabla que he elegido por defecto.

    ./raiz_proyecto/setup/database.sh

### Casos de uso:
-Insertar datos

    ./bin/spark-submit --class mafia.MafiaSystem --master local[*] <path jar>  --code_operation 1 --data_path src/resources/mafia.csv
    
-Desactivar mafioso

    ./bin/spark-submit --class mafia.MafiaSystem --master local[*] <path jar>  --code_operation 2 --dni_member 3
    
-Activar mafioso

    ./bin/spark-submit --class mafia.MafiaSystem --master local[*] <path jar>  --code_operation 3 --dni_member 3
    
-Obtener la lista de jefes con más 50 subordinados

    ./bin/spark-submit --class mafia.MafiaSystem --master local[*] <path jar>  --code_operation 4
    
### Comentarios
He utilizado Spark porque entiendo que al ser una posición Big Data se valora el uso de tecnologías relacionadas. Pero para este caso, siendo una cantidad de datos pequeña, creo que podría haber soluciones más simples y eficientes. 